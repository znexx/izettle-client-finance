# PayoutInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_balance** | **int** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**next_payout_amount** | **int** |  | [optional] 
**discount_remaining** | **int** |  | [optional] 
**periodicity** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


