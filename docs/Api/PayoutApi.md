# Swagger\Client\PayoutApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPayoutInfo**](PayoutApi.md#getPayoutInfo) | **GET** /organizations/{organizationUUID}/payout-info | Retrieves payout-related information for an organization.


# **getPayoutInfo**
> \Swagger\Client\Model\PayoutInfo getPayoutInfo($organization_uuid, $at)

Retrieves payout-related information for an organization.

Response is wrapped in a response envelope, actual response is available from the 'data' field

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | the UUID for the organization, or \"self\" to denote that the organization should be derived from the authenticated user.
$at = "at_example"; // string | used to get information for a specific point in history (ignoring any later transactions).

try {
    $result = $apiInstance->getPayoutInfo($organization_uuid, $at);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayoutApi->getPayoutInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | **string**| the UUID for the organization, or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user. |
 **at** | **string**| used to get information for a specific point in history (ignoring any later transactions). | [optional]

### Return type

[**\Swagger\Client\Model\PayoutInfo**](../Model/PayoutInfo.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

