# Swagger\Client\AccountsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccountTransactions**](AccountsApi.md#getAccountTransactions) | **GET** /organizations/{organizationUUID}/accounts/{accountTypeGroup}/transactions | Retrieves a list of transactions affecting an account.
[**getBalance**](AccountsApi.md#getBalance) | **GET** /organizations/{organizationUUID}/accounts/{accountTypeGroup}/balance | Retrieves the balance of an account.


# **getAccountTransactions**
> \Swagger\Client\Model\AccountTransaction getAccountTransactions($organization_uuid, $account_type_group, $include_transaction_type, $start, $end, $limit, $offset)

Retrieves a list of transactions affecting an account.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | the UUID for the organization, or \"self\" to denote that the organization should be derived from the authenticated user
$account_type_group = "account_type_group_example"; // string | which account type to get data from. Either LIQUID or PRELIMINARY.
$include_transaction_type = array("include_transaction_type_example"); // string[] | the transaction types to include in the response. Multiple values allowed.
$start = "start_example"; // string | a start point in time, limiting the result set (inclusive). ISO 8601 Datetime given in UTC.
$end = "end_example"; // string | an end point in time, limiting the result set (exclusive). ISO 8601 Datetime given in UTC.
$limit = 789; // int | limits the result set to X number of transactions.
$offset = 789; // int | offsets the result set by X number of transactions.

try {
    $result = $apiInstance->getAccountTransactions($organization_uuid, $account_type_group, $include_transaction_type, $start, $end, $limit, $offset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->getAccountTransactions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | **string**| the UUID for the organization, or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user |
 **account_type_group** | **string**| which account type to get data from. Either LIQUID or PRELIMINARY. |
 **include_transaction_type** | [**string[]**](../Model/string.md)| the transaction types to include in the response. Multiple values allowed. | [optional]
 **start** | **string**| a start point in time, limiting the result set (inclusive). ISO 8601 Datetime given in UTC. | [optional]
 **end** | **string**| an end point in time, limiting the result set (exclusive). ISO 8601 Datetime given in UTC. | [optional]
 **limit** | **int**| limits the result set to X number of transactions. | [optional]
 **offset** | **int**| offsets the result set by X number of transactions. | [optional]

### Return type

[**\Swagger\Client\Model\AccountTransaction**](../Model/AccountTransaction.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBalance**
> \Swagger\Client\Model\BalanceInfo getBalance($organization_uuid, $account_type_group, $at)

Retrieves the balance of an account.

Retrieves either the liquid or preliminary balance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID or or \"self\" to denote that the organization should be derived from the authenticated user
$account_type_group = "account_type_group_example"; // string | which account type to get data from. Either LIQUID or PRELIMINARY
$at = "at_example"; // string | used to get the balance at a specific point in history (ignoring any later transactions)

try {
    $result = $apiInstance->getBalance($organization_uuid, $account_type_group, $at);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->getBalance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | **string**| Organization Identifier as an UUID or or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user |
 **account_type_group** | **string**| which account type to get data from. Either LIQUID or PRELIMINARY |
 **at** | **string**| used to get the balance at a specific point in history (ignoring any later transactions) | [optional]

### Return type

[**\Swagger\Client\Model\BalanceInfo**](../Model/BalanceInfo.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

