<?php
/**
 * BalanceInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API documentation for iZettle Finance Service
 *
 * The Finance service contains information about monetary transactions where money has passed through iZettle, such transactions can be for example card payments, card fees and payouts to the customer.
 *
 * OpenAPI spec version: v1.0
 * Contact: api@izettle.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * BalanceInfoTest Class Doc Comment
 *
 * @category    Class
 * @description BalanceInfo
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class BalanceInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "BalanceInfo"
     */
    public function testBalanceInfo()
    {
    }

    /**
     * Test attribute "total_balance"
     */
    public function testPropertyTotalBalance()
    {
    }

    /**
     * Test attribute "currency_id"
     */
    public function testPropertyCurrencyId()
    {
    }
}
