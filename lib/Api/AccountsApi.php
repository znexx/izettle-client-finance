<?php
/**
 * AccountsApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API documentation for iZettle Finance Service
 *
 * The Finance service contains information about monetary transactions where money has passed through iZettle, such transactions can be for example card payments, card fees and payouts to the customer.
 *
 * OpenAPI spec version: v1.0
 * Contact: api@izettle.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Swagger\Client\ApiException;
use Swagger\Client\Configuration;
use Swagger\Client\HeaderSelector;
use Swagger\Client\ObjectSerializer;

/**
 * AccountsApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AccountsApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation getAccountTransactions
     *
     * Retrieves a list of transactions affecting an account.
     *
     * @param  string $organization_uuid the UUID for the organization, or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY. (required)
     * @param  string[] $include_transaction_type the transaction types to include in the response. Multiple values allowed. (optional)
     * @param  string $start a start point in time, limiting the result set (inclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  string $end an end point in time, limiting the result set (exclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  int $limit limits the result set to X number of transactions. (optional)
     * @param  int $offset offsets the result set by X number of transactions. (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Swagger\Client\Model\AccountTransaction
     */
    public function getAccountTransactions($organization_uuid, $account_type_group, $include_transaction_type = null, $start = null, $end = null, $limit = null, $offset = null)
    {
        list($response) = $this->getAccountTransactionsWithHttpInfo($organization_uuid, $account_type_group, $include_transaction_type, $start, $end, $limit, $offset);
        return $response;
    }

    /**
     * Operation getAccountTransactionsWithHttpInfo
     *
     * Retrieves a list of transactions affecting an account.
     *
     * @param  string $organization_uuid the UUID for the organization, or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY. (required)
     * @param  string[] $include_transaction_type the transaction types to include in the response. Multiple values allowed. (optional)
     * @param  string $start a start point in time, limiting the result set (inclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  string $end an end point in time, limiting the result set (exclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  int $limit limits the result set to X number of transactions. (optional)
     * @param  int $offset offsets the result set by X number of transactions. (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Swagger\Client\Model\AccountTransaction, HTTP status code, HTTP response headers (array of strings)
     */
    public function getAccountTransactionsWithHttpInfo($organization_uuid, $account_type_group, $include_transaction_type = null, $start = null, $end = null, $limit = null, $offset = null)
    {
        $returnType = '\Swagger\Client\Model\AccountTransaction';
        $request = $this->getAccountTransactionsRequest($organization_uuid, $account_type_group, $include_transaction_type, $start, $end, $limit, $offset);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if ($returnType !== 'string') {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\AccountTransaction',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getAccountTransactionsAsync
     *
     * Retrieves a list of transactions affecting an account.
     *
     * @param  string $organization_uuid the UUID for the organization, or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY. (required)
     * @param  string[] $include_transaction_type the transaction types to include in the response. Multiple values allowed. (optional)
     * @param  string $start a start point in time, limiting the result set (inclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  string $end an end point in time, limiting the result set (exclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  int $limit limits the result set to X number of transactions. (optional)
     * @param  int $offset offsets the result set by X number of transactions. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getAccountTransactionsAsync($organization_uuid, $account_type_group, $include_transaction_type = null, $start = null, $end = null, $limit = null, $offset = null)
    {
        return $this->getAccountTransactionsAsyncWithHttpInfo($organization_uuid, $account_type_group, $include_transaction_type, $start, $end, $limit, $offset)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getAccountTransactionsAsyncWithHttpInfo
     *
     * Retrieves a list of transactions affecting an account.
     *
     * @param  string $organization_uuid the UUID for the organization, or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY. (required)
     * @param  string[] $include_transaction_type the transaction types to include in the response. Multiple values allowed. (optional)
     * @param  string $start a start point in time, limiting the result set (inclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  string $end an end point in time, limiting the result set (exclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  int $limit limits the result set to X number of transactions. (optional)
     * @param  int $offset offsets the result set by X number of transactions. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getAccountTransactionsAsyncWithHttpInfo($organization_uuid, $account_type_group, $include_transaction_type = null, $start = null, $end = null, $limit = null, $offset = null)
    {
        $returnType = '\Swagger\Client\Model\AccountTransaction';
        $request = $this->getAccountTransactionsRequest($organization_uuid, $account_type_group, $include_transaction_type, $start, $end, $limit, $offset);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getAccountTransactions'
     *
     * @param  string $organization_uuid the UUID for the organization, or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY. (required)
     * @param  string[] $include_transaction_type the transaction types to include in the response. Multiple values allowed. (optional)
     * @param  string $start a start point in time, limiting the result set (inclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  string $end an end point in time, limiting the result set (exclusive). ISO 8601 Datetime given in UTC. (optional)
     * @param  int $limit limits the result set to X number of transactions. (optional)
     * @param  int $offset offsets the result set by X number of transactions. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getAccountTransactionsRequest($organization_uuid, $account_type_group, $include_transaction_type = null, $start = null, $end = null, $limit = null, $offset = null)
    {
        // verify the required parameter 'organization_uuid' is set
        if ($organization_uuid === null || (is_array($organization_uuid) && count($organization_uuid) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $organization_uuid when calling getAccountTransactions'
            );
        }
        // verify the required parameter 'account_type_group' is set
        if ($account_type_group === null || (is_array($account_type_group) && count($account_type_group) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $account_type_group when calling getAccountTransactions'
            );
        }

        $resourcePath = '/organizations/{organizationUUID}/accounts/{accountTypeGroup}/transactions';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if (is_array($include_transaction_type)) {
            $queryParams['includeTransactionType'] = $include_transaction_type;
        } else
        if ($include_transaction_type !== null) {
            $queryParams['includeTransactionType'] = ObjectSerializer::toQueryValue($include_transaction_type);
        }
        // query params
        if ($start !== null) {
            $queryParams['start'] = ObjectSerializer::toQueryValue($start);
        }
        // query params
        if ($end !== null) {
            $queryParams['end'] = ObjectSerializer::toQueryValue($end);
        }
        // query params
        if ($limit !== null) {
            $queryParams['limit'] = ObjectSerializer::toQueryValue($limit);
        }
        // query params
        if ($offset !== null) {
            $queryParams['offset'] = ObjectSerializer::toQueryValue($offset);
        }

        // path params
        if ($organization_uuid !== null) {
            $resourcePath = str_replace(
                '{' . 'organizationUUID' . '}',
                ObjectSerializer::toPathValue($organization_uuid),
                $resourcePath
            );
        }
        // path params
        if ($account_type_group !== null) {
            $resourcePath = str_replace(
                '{' . 'accountTypeGroup' . '}',
                ObjectSerializer::toPathValue($account_type_group),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            
            if($headers['Content-Type'] === 'application/json') {
                // \stdClass has no __toString(), so we should encode it manually
                if ($httpBody instanceof \stdClass) {
                    $httpBody = \GuzzleHttp\json_encode($httpBody);
                }
                // array has no __toString(), so we should encode it manually
                if(is_array($httpBody)) {
                    $httpBody = \GuzzleHttp\json_encode(ObjectSerializer::sanitizeForSerialization($httpBody));
                }
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }

        // this endpoint requires OAuth (access token)
        if ($this->config->getAccessToken() !== null) {
            $headers['Authorization'] = 'Bearer ' . $this->config->getAccessToken();
        }

        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation getBalance
     *
     * Retrieves the balance of an account.
     *
     * @param  string $organization_uuid Organization Identifier as an UUID or or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY (required)
     * @param  string $at used to get the balance at a specific point in history (ignoring any later transactions) (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Swagger\Client\Model\BalanceInfo
     */
    public function getBalance($organization_uuid, $account_type_group, $at = null)
    {
        list($response) = $this->getBalanceWithHttpInfo($organization_uuid, $account_type_group, $at);
        return $response;
    }

    /**
     * Operation getBalanceWithHttpInfo
     *
     * Retrieves the balance of an account.
     *
     * @param  string $organization_uuid Organization Identifier as an UUID or or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY (required)
     * @param  string $at used to get the balance at a specific point in history (ignoring any later transactions) (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Swagger\Client\Model\BalanceInfo, HTTP status code, HTTP response headers (array of strings)
     */
    public function getBalanceWithHttpInfo($organization_uuid, $account_type_group, $at = null)
    {
        $returnType = '\Swagger\Client\Model\BalanceInfo';
        $request = $this->getBalanceRequest($organization_uuid, $account_type_group, $at);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if ($returnType !== 'string') {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\BalanceInfo',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getBalanceAsync
     *
     * Retrieves the balance of an account.
     *
     * @param  string $organization_uuid Organization Identifier as an UUID or or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY (required)
     * @param  string $at used to get the balance at a specific point in history (ignoring any later transactions) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getBalanceAsync($organization_uuid, $account_type_group, $at = null)
    {
        return $this->getBalanceAsyncWithHttpInfo($organization_uuid, $account_type_group, $at)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getBalanceAsyncWithHttpInfo
     *
     * Retrieves the balance of an account.
     *
     * @param  string $organization_uuid Organization Identifier as an UUID or or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY (required)
     * @param  string $at used to get the balance at a specific point in history (ignoring any later transactions) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getBalanceAsyncWithHttpInfo($organization_uuid, $account_type_group, $at = null)
    {
        $returnType = '\Swagger\Client\Model\BalanceInfo';
        $request = $this->getBalanceRequest($organization_uuid, $account_type_group, $at);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getBalance'
     *
     * @param  string $organization_uuid Organization Identifier as an UUID or or \&quot;self\&quot; to denote that the organization should be derived from the authenticated user (required)
     * @param  string $account_type_group which account type to get data from. Either LIQUID or PRELIMINARY (required)
     * @param  string $at used to get the balance at a specific point in history (ignoring any later transactions) (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getBalanceRequest($organization_uuid, $account_type_group, $at = null)
    {
        // verify the required parameter 'organization_uuid' is set
        if ($organization_uuid === null || (is_array($organization_uuid) && count($organization_uuid) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $organization_uuid when calling getBalance'
            );
        }
        // verify the required parameter 'account_type_group' is set
        if ($account_type_group === null || (is_array($account_type_group) && count($account_type_group) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $account_type_group when calling getBalance'
            );
        }

        $resourcePath = '/organizations/{organizationUUID}/accounts/{accountTypeGroup}/balance';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($at !== null) {
            $queryParams['at'] = ObjectSerializer::toQueryValue($at);
        }

        // path params
        if ($organization_uuid !== null) {
            $resourcePath = str_replace(
                '{' . 'organizationUUID' . '}',
                ObjectSerializer::toPathValue($organization_uuid),
                $resourcePath
            );
        }
        // path params
        if ($account_type_group !== null) {
            $resourcePath = str_replace(
                '{' . 'accountTypeGroup' . '}',
                ObjectSerializer::toPathValue($account_type_group),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            
            if($headers['Content-Type'] === 'application/json') {
                // \stdClass has no __toString(), so we should encode it manually
                if ($httpBody instanceof \stdClass) {
                    $httpBody = \GuzzleHttp\json_encode($httpBody);
                }
                // array has no __toString(), so we should encode it manually
                if(is_array($httpBody)) {
                    $httpBody = \GuzzleHttp\json_encode(ObjectSerializer::sanitizeForSerialization($httpBody));
                }
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }

        // this endpoint requires OAuth (access token)
        if ($this->config->getAccessToken() !== null) {
            $headers['Authorization'] = 'Bearer ' . $this->config->getAccessToken();
        }

        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
